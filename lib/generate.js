/*
 * generate函数，用来处理异步请求
 */

/* 最简单的generate */
function* gen() {
   return 'my generate'
}

/* yield构建generate sequence
 * yield：暂停执行，并返回yield之后的值；
 * */
function* genSequence() {
    yield 'first';
    yield 'second';
    yield 'third';
}

/* 给generate函数赋值 */
function* genSetValue() {
    let data1 = yield ;
    let data2 = yield ;
    console.log('data1: ' + data1 + '. data2: ' + data2);
}

/* 先写一个常见的回调函数 */
const printData1 = () => {
    let result = 0;
    let print = (something) => console.log(something);
    setTimeout(() => reset(result, print), 1000);
}

const reset = (result, callback) => {
    result = 'data1';
    callback(result);
}
/* 如果想在一秒后给result重新赋值然后输出result，
   只能将print作为reset的回调函数传入reset中。
   但其实reset函数只应该给result重新赋值，并不应该做输出的事情。
   而且在一些复杂业务下，这样的写法很容易造成回调函数嵌套，形成回调地狱。
   */
let asyncFn = async()
function* async() {
    let result = yield resetResult();
    console.log(result);
}

const resetResult = () => {
    setTimeout(function () {
        asyncFn.next('data1')
    }, 1000)
}

/* function* 标记方法为一个generate函数
 *
 *
 * */

const Generate = {
    gen1: gen(),
    gen2: gen(),
    genSequence1: genSequence(),
    genSequence2: genSequence(),
    genSetValue: genSetValue(),
    printData1: printData1,
    asyncFn: asyncFn
}


export {Generate}