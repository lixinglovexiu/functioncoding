/* 函子，一个对象实现了map函数，在遍历每个对象值时生成一个新的对象 */

/* Maybe 处理可能为null或undefined的情况, 当为空时输出空值 */
const Maybe = function (value) {
    this.value = value;
}

Maybe.of = function (value) {
    return new Maybe(value);
}

Maybe.prototype.isNothing = function () {
    return (this.value === null || this.value === undefined)
}

Maybe.prototype.map = function (fn) {
    return this.isNothing() ? Maybe.of(null) : Maybe.of(fn(this.value))
}

/* Either 当值为空时，输出错误信息 */
const Some = function (value) {
    this.value = value;
}

Some.of = function (value) {
    return new Some(value);
}

Some.prototype.map = function (fn) {
    return Some.of(fn(this.value))
}

const Nothing = function (value) {
    this.value = value;
}

Nothing.of = function (value) {
    return new Nothing(value);
}

Nothing.prototype.map = function (fn) {
    return this
}

const Either = {
    Some: Some,
    Nothing: Nothing
}

const Functor = {
    Maybe: Maybe,
    Either: Either
}


export {Functor}