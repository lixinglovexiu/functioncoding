/* 柯里化 完成fun(a,b,c) => fun(a)(b)(c)转换 */
const curry = (fn) => {
    if (typeof(fn) !== 'function') {
        throw Error(fn + ' is not a function')
    }

    return function curryFn(...args) {
        // 如果传入参数的长度小于fn参数的长度，就递归增加参数
        if (args.length < fn.length) {
            return (...nextArgs) => curryFn.apply(null, args.concat([].slice.call(nextArgs)))
        } else {
            return fn.apply(null, args)
        }
    }
}

export default curry