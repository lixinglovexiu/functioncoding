const forEach = (array, fn) => {
    for (const data of array)
        fn(data)
}

const filter = (array, fn) => {
    const result = []
    for (let value of array)
        fn(value) ? result.push(value) : undefined
    return result
}

const reduce = (array, fn) => {
    let tmp = array[0]
    for (let i = 1; i < array.length; i++) {
        tmp = fn(tmp, array[i])
    }
    return tmp
}

/*
    tap(value)(fn)
    执行fn(value)，并输出value
    (exp1, exp2)的含义是执行exp1和exp2，返回exp2的结果
    */
const tap = (value) =>
    (fn) => (typeof(fn) === 'function' && fn(value), console.log(value))

/*
    如果方法接收多个参数，将方法转换为只接收一个参数
    */
const unary = (fn) =>
    fn.length === 1 ? fn : (arg) => fn(arg)

/* 把嵌套数组连接到一个数组中 */
const concatSelf = (array) => {
    let result = []
    for (let value of array)
        // 这里使用apply，如果value是一个数组就会被当做多个参数
        result.push.apply(result, value)
    return result
}

/* 合并两个数组 */
const concat = (leftArray, rightArray, fn) => {
    let len = Math.min(leftArray.length, rightArray.length)
    const result = []
    for (let i = 0; i < len; i++) {
        result.push(fn(leftArray[i], rightArray[i]))
    }
    return result
}


const ArrayUtils = {
    forEach: forEach,
    filter: filter,
    reduce: reduce,
    tap: tap,
    unary: unary,
    concatSelf: concatSelf,
    concat: concat,
}

export {ArrayUtils}