/* 函数组合 compose(fn1, fn2, fn3) = (param) => fn1(fn2(fn3(param))) */
import {ArrayUtils} from "./array_util";

const compose = (...fns) => {
    return (param) => {
        return ArrayUtils.reduce(
            fns.reverse(), (fn1, fn2) => {
                if (typeof(fn1) === 'function') {
                    return fn2(fn1(param));
                } else {
                    return fn2(fn1);
                }

            }
        );
    }
}

export default compose