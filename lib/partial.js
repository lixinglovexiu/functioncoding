/* 偏函数 完成参数顺序的转换 */
const partial = (fn, ...params) => {
    return (...args) => {
        let count = 0
        for (let i = 0; i < params.length; i++) {
            if (params[i] === undefined) {
                params[i] = args[count];
                count++;
            }
        }
        return fn.apply(null, params)
    }


}

export default partial