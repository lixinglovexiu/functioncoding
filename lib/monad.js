/* Monad函子，包含chain方法，返回当前对象执行map操作后的value值 */

/* Maybe 处理可能为null或undefined的情况, 当为空时输出空值 */
const Maybe = function (value) {
    this.value = value;
}

Maybe.of = function (value) {
    return new Maybe(value);
}

Maybe.prototype.isNothing = function () {
    return (this.value === null || this.value === undefined)
}

Maybe.prototype.map = function (fn) {
    return this.isNothing() ? Maybe.of(null) : Maybe.of(fn(this.value))
}

Maybe.prototype.join = function () {
    return this.value;
}

Maybe.prototype.chain = function (fn) {
    return this.map(fn).join();
}


const Monad = {
    Maybe: Maybe,
}


export {Monad}