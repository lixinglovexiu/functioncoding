/* 只执行一次 */
const once = (fn) => {
    let done = false
    return () => {
        return done ? undefined : ((done = true), fn.apply(this, arguments))
    }
}

/* 记录执行结果 */
const memorized = (fn) => {
    let resultTable = {}
    return (arg) => resultTable[arg] || (resultTable[arg] = fn(arg))
}

const CommonUtils = {
    once: once
}

export {CommonUtils}