const sortBy = (param) => {
    return (a, b) => {
       return a[param] > b[param] ? 1 : a[param] < b[param] ? -1 : 0
    }
}

export default sortBy