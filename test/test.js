import {ArrayUtils} from '../lib/array_util.js'
import sortBy from '../lib/compare_util.js'
import curry from '../lib/curry.js'
import partial from '../lib/partial.js'
import compose from "../lib/compose";
import {Functor} from "../lib/functor";
import {Monad} from "../lib/monad";
import asyncFn, {Generate} from "../lib/generate";

/*// forEach
let array = [1, 2, 3]
ArrayUtils.forEach(array, (data) => console.log(data))

// sortBy
let apples = [
    {'name': 'hongfushi', 'price': 100},
    {'name': 'sheguo', 'price': 50},
]

console.log(apples.sort(sortBy('price')))

// tap
ArrayUtils.forEach(array, (data) => ArrayUtils.tap(data)(
    (data) => console.log('test', data)
))

// unary
let str_array = ['1', '2', '3']
console.log(str_array.map(ArrayUtils.unary(parseInt)))

// filter
console.log(ArrayUtils.filter(array, (arg) => arg > 2))

// concatSelf
console.log(ArrayUtils.concatSelf(
    [
        [1, 2, 3],
        [4, 5, 6]
    ]
))

// reduce
console.log(ArrayUtils.reduce(array, (a, b) => a * a + b * b))

// concat
console.log(ArrayUtils.concat(
    [1, 2, 3],
    [4, 5, 6],
    (a, b) => a + b
))
// curry
const add = (x, y, z) => x + y + z
const test = curry(add)
console.log(test(3)(2)(4))
console.log(test(3)(2, 4))

// partial, 需要注意！partial处理的函数只能使用一次。下面的代码输出 aaaa, aaaa, bbbb
const partial_timeout = partial(setTimeout, undefined, 3000)
const partial_timeout1 = partial(setTimeout, undefined, 3000)
partial_timeout(() => console.log('aaaa'))
partial_timeout(() => console.log('bbbb'))
partial_timeout1(() => console.log('bbbb'))

// compose
const double = (x) => 2 * x
const triple = (x) => 3 * x
const six_times = compose(double, triple)
const twelve_times = compose(double, double, triple)
console.log(six_times(4))
console.log(twelve_times(4))

// Maybe
const getCall = (firstName) => Functor.Maybe.of(firstName)
    .map((x) => x.toUpperCase())
    .map((x) => 'Mr.' + x)

console.log(getCall('li'))
console.log(getCall(null))
console.log(getCall(undefined))

// Either
const parseJsonEither = (jsonStr) => {
    try {
        return Functor.Either.Some.of(JSON.parse(jsonStr))
    } catch (e) {
        return Functor.Either.Nothing.of('JsonParseError, input is ' + jsonStr)
    }
}

const json1 = '{"name":"lixing"}';
const json2 = "{ name: 'lixing' }";

console.log(
    parseJsonEither(json1)
        .map((x) => x['name'])
        .map((x) => x.toUpperCase())
)

console.log(
    parseJsonEither(json2)
        .map((x) => x['name'])
        .map((x) => x.toUpperCase())
)

// Monad
const monadGetCall = (firstName) => Monad.Maybe.of(firstName)
    .map((x) => x.toUpperCase())
    .chain((x) => 'Mr.' + x)

console.log(monadGetCall('li'))
console.log(monadGetCall(null))
console.log(monadGetCall(undefined))
*/

// Generate
console.log(Generate.gen1.next());
console.log(Generate.gen1.next().value);
console.log(Generate.gen2.next().value);

console.log(Generate.genSequence1.next().value);
console.log(Generate.genSequence1.next().value);
console.log(Generate.genSequence1.next().value);

for (let value of Generate.genSequence2) {
    console.log(value)
}

Generate.genSetValue.next();
Generate.genSetValue.next('I am data1');
Generate.genSetValue.next('I am data2');

Generate.printData1();
Generate.asyncFn.next();


